//
//  KeystokTests.m
//  KeystokTests
//
//  Created by Jacek Grygiel on 03/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "KeystokImports.h"
#import "APIService.h"
#import "XCTestCase+AsyncTesting.h"
@interface KeystokTests : XCTestCase

@end

@implementation KeystokTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//- (void) testDecryptionAllData{
//    
//    [[APIService sharedInstance] getAllDecryptedValuesWithKey:@"eyJpZCI6NDIsInJ0IjoiMzNiMjJjMTRjM2I0ODFiODM0NWE2N2Y5ZDQ5NWU3MzI5Njk1ZDVkMmQ2ZWZhYmExNTlmOGU3Mzk4ZGExYThkMCIsImRrIjoiNTMwZWU1NDgyOGQ3ZDIzNzcwYjE2ZjIwYjkwZjJjOTVlODI0ODBlMmMxOThkMjAxYzkwMjYxYWViODZhOGViYiIsImFwaV9ob3N0IjoiaHR0cHM6Ly9hcGktc3RhZ2luZy5rZXlzdG9rLmNvbSIsImF1dGhfaG9zdCI6Imh0dHBzOi8vc3RhZ2luZy5rZXlzdG9rLmNvbSJ9" completion:^(id fetchedObject) {
//        
//        MKLog(@"%@", fetchedObject);
//        
//        [self notify:XCTAsyncTestCaseStatusSucceeded];
//        
//    } error:^(NSError *error) {
//        [self notify:XCTAsyncTestCaseStatusFailed];
//
//    }];
//    
//    [self waitForStatus: XCTAsyncTestCaseStatusSucceeded timeout:10];
//
//}




- (void) testDecryptionKeyForOneApplication{
    
    
    APIService *service = [[APIService alloc] init];

    [service getDecryptedValueWithConnectionString:@"eyJpZCI6NDksInJ0IjoiZjRjYmQ5NTM2MjNlNWE0NThkYjMyOWIyNGViY2RiYWJmODBkMzY2ZmM5MTk3NTkzYThiMjk5MDZjODNkZTgxYSIsImRrIjoiOGE3ODU3YjIzODViZDRlNWIwZWI2ZWE5MjhiN2ZmOTNkOTg0MmQ2Y2YwMjY3ODE5ZTQ1OTA4MTg5MmQ4OGQ4ZiJ9" andKey:@"0001" completion:^(id fetchedObject) {
        
        MKLog(@"%@", fetchedObject);

        [self notify:XCTAsyncTestCaseStatusSucceeded];

    } error:^(NSError *error) {
        [self notify:XCTAsyncTestCaseStatusFailed];

    }];
    
    [self waitForStatus: XCTAsyncTestCaseStatusSucceeded timeout:10];
    
}

@end
