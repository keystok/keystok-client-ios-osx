# Description:

Keystok OSX/iOS framework is dedicated for any project which used Objective C. Framework provides functions for managment Keystok configurations.

## Installation:

Project requires installation of [iOS-Universal-Framework](https://github.com/kstenerud/iOS-Universal-Framework).

To install, run the install.sh script in either the **"Real Framework"** or **"Fake Framework"** folder (or both).

To update the Keystok OSX/iOS framework :

Close your project
run the project update script from shell:

    $ ./update_project.py ~/Projects/MyProj/MyProj.xcodeproj/project.pbxproj

and reopen your project


**Import to your project:**

For OSX:

1. Build Keystok with selecting target **"Keystok Mac"**
2. Copy framework **"Keystok.framework"** from **"/Library/Developer/Xcode/DerivedData/Keystok/Build/Products/Debug/"** to your project.
3. Compile your project.

For IOS:

1. Build Keystok with selecting target **"Keystok IOS"**
2. Copy framework **"Keystok-IOS.framework"** from **"/Library/Developer/Xcode/DerivedData/Keystok/Build/Products/Debug-iphoneos"** to your project.
3. Compile your project.


## How to use:

When framework is added to your project, please add

    #import <Keystok-IOS/APIService.h>  // for iOS project
    #import <Keystok/APIService.h> //for OSX project


then create APIService object

    APIService *service = [[APIService alloc] init];

and run method with parameters **"getAllDecryptedValuesWithConnectionString"** to get all appropriate values

    [service getAllDecryptedValuesWithConnectionString:@"connection string" completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError;


or use **"getAllDecryptedValuesWithConnectionString"** for particular key

    [service getDecryptedValueWithConnectionString:@"connection string" andKey:@"some_key" completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError];

completion handler provides id fetchedObject which is an object of NSDictionary class

    typedef void(^CompletionHandler)(id fetchedObject);

