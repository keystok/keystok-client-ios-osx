//
//  NSURLConnection+Addition.h
//  ManakeyPOC
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^CompletionHandler)(id fetchedObject);
typedef void(^ErrorHandler)(NSError * error);

@interface NSURLConnection (Addition)
+ (void) asynchronousRequestWithURL:(NSURL*) url completion:(CompletionHandler) completion error:(ErrorHandler) error;
+ (void) asynchronousPOSTRequestWithURL:(NSURL*) url completion:(CompletionHandler) completion error:(ErrorHandler) error;
@end
