//
//  NSURLConnection+Addition.m
//  ManakeyPOC
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import "NSURLConnection+Addition.h"

@implementation NSURLConnection (Addition)
+ (void) asynchronousRequestWithURL:(NSURL*) url completion:(CompletionHandler) completion error:(ErrorHandler) error{
    
    MKLog(@"asynchronousRequest");

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];

    MKLog(@"before sendAsynchronousRequest");

   [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
       
       NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
       NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
       NSArray *fetchedObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
       
       MKLog(@"%@", fetchedObject);
       
       if (fetchedObject) {
           
           completion(fetchedObject);
           
       }else if (connectionError){
           
           error(connectionError);
           
       }

       
       
    }];
//    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        
//        
//    }];
    
}

+ (void) asynchronousPOSTRequestWithURL:(NSURL*) url completion:(CompletionHandler) completion error:(ErrorHandler) error{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPMethod: @"POST"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *fetchedObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        
        
        if (fetchedObject) {
            
            completion(fetchedObject);
            
        }else if (connectionError){
            
            error(connectionError);
            
        }
        
    }];
    
}


@end
