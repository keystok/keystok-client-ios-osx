//
//  APIService.h
//  Keystok
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeystokImports.h"

typedef void(^CompletionHandler)(id fetchedObject);
typedef void(^ErrorHandler)(NSError * error);

@interface APIService : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>
@property(nonatomic, strong) NSString *apiUrlPath;
@property(nonatomic, strong) NSString *authUrlPath;

- (id) initServiceWithApiUrl:(NSString*) apiUrl andAuthUrl:(NSString*) authUrl;

//! Send request for encrypted data
/*!
 \param params: need to pass parameters - access_token, dk, expires_in, id, token_type
 \param handlerCompletion run the block with appropriate data
 \params handlerError run the block with error if request failed
 */
- (void) sendRequestForEncryptedDataWithParameters:(NSDictionary*) params completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError;

//! Send request for refresh token
/*!
 \param params: need to pass parameters - grant_type, refresh_token, refresh_token_url_path
 \param handlerCompletion run the block with appropriate data
 \params handlerError run the block with error if request failed
 */

- (void) sendRequestForRefreshTokenWithParameters:(NSDictionary*) params completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError;

//! Get configuration parameters
/*!
 \param key represents string with value - can be used for fetching configuration data
 \return dictionary with all parameters
 */

- (NSDictionary*) getConfigurationDictionaryForKey:(NSString*) key;

//! Get all decrypted key values for appropriate applications
/*!
 \param keyValue represents string with value - can be used for fetching configuration data
 \param handlerCompletion run the block with appropriate data
 \params handlerError run the block with error if request failed
 */
- (void) getAllDecryptedValuesWithConnectionString:(NSString*) keyValue completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError;

//! Get one decrypted key value for appropriate application
/*!
 \param keyValue represents string with value - can be used for fetching configuration data
 \param handlerCompletion run the block with appropriate data
 \params handlerError run the block with error if request failed
 */
- (void) getDecryptedValueWithConnectionString:(NSString*) keyValue andKey:(NSString*) applicationName completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError;

@end
