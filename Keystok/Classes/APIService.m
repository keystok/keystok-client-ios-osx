//
//  APIService.m
//  Keystok
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import "APIService.h"
#import "GTMBase64.h"

@interface APIService ()
{
    @private
    NSDictionary *storedEncryptedData;
}
@property(nonatomic, copy) CompletionHandler tCompletionHandler;
@property(nonatomic, copy) ErrorHandler tErrorHandler;
@end


@implementation APIService
- (id) initServiceWithApiUrl:(NSString*) apiUrl andAuthUrl:(NSString*) authUrl{
    self = [super init];
    if (self) {
        self.apiUrlPath = apiUrl;
        self.authUrlPath = authUrl;
    }
    
    return self;
}


- (void) sendRequestForEncryptedDataWithParameters:(NSDictionary*) params completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError{
    
    NSString *urlPath = [params valueForKey:kRequestKeystokAPI];
    NSString *accessToken = [params valueForKey:kRequestAccessToken];
    NSUInteger identifier = [[params valueForKey:kIdentifierApplication] integerValue];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%lu/deploy?access_token=%@", urlPath, (unsigned long)identifier, accessToken]];
    
    [self asynchronousRequestWithURL:url completion:^(id fetchedObject) {
        MKLog(@"%@", fetchedObject);
        handlerCompletion(fetchedObject);
        
    } error:^(NSError *error) {
        
        
        handlerError(error);
        
    }];
    
    
}
- (void) sendRequestForRefreshTokenWithParameters:(NSDictionary*) params completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError{
    NSString *urlPath = [params valueForKey:kRequestKeystokAuth];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlPath]];
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:[params valueForKey:kRequestRefreshToken],kRequestRefreshToken,
                              [params valueForKey:kRequestGrantType], kRequestGrantType,nil];
    
    NSData *postData = [self encodeDictionary:postDict];
    
    // Create the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [urlConnection start];
    self.tCompletionHandler = handlerCompletion;
    self.tErrorHandler = handlerError;
    
}

- (void) getAllDecryptedValuesWithConnectionString:(NSString*) keyValue completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError{
    
    MKLog(@"getAllDecryptedValuesWithKey");
    
    NSDictionary *config = [self getConfigurationDictionaryForKey:keyValue];
    NSString *apiHost = [config valueForKey:kRequestKeystokAPI];
    NSString *authHost = [config valueForKey:kRequestKeystokAuth];
    
    if ([config valueForKey:@"api_host"]) {
        apiHost = [NSString stringWithFormat:@"%@/%@", [config valueForKey:kRequestKeystokAPI],kRequestKeystokAPIDestPath];
    }else if(self.apiUrlPath){
        apiHost = self.apiUrlPath;
    }else{
        apiHost =[NSString stringWithFormat:@"%@/%@", kRequestKeystokAPIPath,kRequestKeystokAPIDestPath];

    }
    
    if ([config valueForKey:@"auth_host"]) {
        authHost = [NSString stringWithFormat:@"%@/%@", [config valueForKey:kRequestKeystokAuth],KRequestKeystokAuthDestPath];
    }else if(self.authUrlPath){
        authHost = self.authUrlPath;
    }else{
        authHost =[NSString stringWithFormat:@"%@/%@", kRequestKeystokPath,KRequestKeystokAuthDestPath];

    }
    
    
    MKLog(@"%@, %@", authHost, apiHost);
    [self sendRequestForRefreshTokenWithParameters:@{kRequestKeystokAuth:authHost, kRequestRefreshToken:[config valueForKey:kRefreshToken], kRequestGrantType:kRequestRefreshToken} completion:^(id fetchedObject) {
        MKLog(@"%@", fetchedObject);
        
        NSMutableDictionary *params = (NSMutableDictionary*) [fetchedObject mutableCopy];
        [params setValue:[config valueForKey:kIdentifierApplication] forKeyPath:kIdentifierApplication];
        [params setValue:[config valueForKey:kDecriptionKey] forKeyPath:kDecriptionKey];
        [params setValue:apiHost forKey:kRequestKeystokAPI];
        
        MKLog(@"before sendRequest");

        [self sendRequestForEncryptedDataWithParameters:params completion:^(id fetchedObject) {
                
                NSArray *dicts = [fetchedObject allValues];
                
                NSMutableDictionary *allDecryptedData = [[NSMutableDictionary alloc] init];
                for (NSDictionary *dict in dicts) {
                    NSString *txtFilePath = [(NSString*) dict valueForKey:@"key"];
                    
                    NSDictionary *json = [[CryptManager sharedInstance] parsedJsonEncryptedData:txtFilePath];
                    
                    NSString *decryptedData = [[CryptManager sharedInstance] decryptDataFromJSONDictionary:json withKeyValue:[params valueForKey:kDecriptionKey]];
                    
                    
                    [allDecryptedData setValue:decryptedData forKey: [fetchedObject allKeysForObject:dict][0]];
                    
                }
                storedEncryptedData = allDecryptedData;
                handlerCompletion(allDecryptedData);
                
                
            } error:^(NSError *error) {
                
                handlerError(error);
            }];
        
        
    } error:^(NSError *error) {
        handlerError(error);
    }];

}

- (void) getDecryptedValueWithConnectionString:(NSString*) keyValue andKey:(NSString*) applicationName completion:(CompletionHandler) handlerCompletion error:(ErrorHandler) handlerError{
    NSString *decryptedKey = [storedEncryptedData valueForKey:applicationName];
    if (decryptedKey) {
        handlerCompletion(decryptedKey);
    }else{
        [self getAllDecryptedValuesWithConnectionString:keyValue completion:^(id fetchedObject) {
            handlerCompletion([fetchedObject valueForKey:applicationName]);
        } error:^(NSError *error) {
            handlerError(error);
        }];
    }
    
}

#pragma mark Helper methods

- (NSDictionary*) getConfigurationDictionaryForKey:(NSString*) key{
    
    NSAssert(key!=nil, @"Key value need to be passed");
    
    NSData *jsonData = [YAJL_GTMBase64 decodeString:key];
    
    NSDictionary *jsonObject=[NSJSONSerialization
                              JSONObjectWithData:jsonData
                              options:NSJSONReadingMutableLeaves
                              error:nil];
    
    return jsonObject;
}

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {

    NSHTTPURLResponse *urlResponseHttp = (NSHTTPURLResponse*) response;
    
    if ([urlResponseHttp statusCode] != 200 ) {
        self.tErrorHandler([NSError errorWithDomain:@"Received response failed" code:urlResponseHttp.statusCode userInfo:nil]);
    }

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data
                          options:NSJSONReadingMutableLeaves
                          error:&error];
    
    if (self.tCompletionHandler) {
        self.tCompletionHandler(json);
    }
}



- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        if ([@"keystok.com" isEqual:challenge.protectionSpace.host])
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}



#pragma mark Asynch requests

- (void) asynchronousRequestWithURL:(NSURL*) url completion:(CompletionHandler) completion error:(ErrorHandler) error{
    
    MKLog(@"asynchronousRequest");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *fetchedObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        
        if (fetchedObject) {
            
            completion(fetchedObject);
            
        }else if (connectionError){
            
            error(connectionError);
            
        }
        
        
        
    }];
    
}

@end
