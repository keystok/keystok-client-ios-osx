//
//  CryptManager.m
//  Keystok
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import "CryptManager.h"
#import "GTMBase64.h"


@implementation CryptManager

+ (id) sharedInstance{
    
    static dispatch_once_t once;
    static id shared;
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
        
    });
    return shared;
    
}

- (NSString*) decryptDataFromJSONDictionary:(NSDictionary*) json withKeyValue:(NSString*) keyValue{
    
    NSString *ctJson = [json valueForKey:kDecryptionCT];
    NSString *vectorJson = [json valueForKey:kDecryptionIV];
    NSString *saltJson = [json valueForKey:kDecryptionSalt];
    uint iterations = (uint)[[json valueForKey:kDecryptionIter] unsignedIntegerValue];
    
    NSString *ct  = ctJson;
    
    NSData *data = [YAJL_GTMBase64 decodeString:ct];
    
    NSData *salt = [YAJL_GTMBase64 decodeString:saltJson];
    
    NSData *keyData = [[SHA1Service sharedInstance] SHA1KeyForPassword:keyValue salt:salt iterations:iterations];
    
    NSData *descrypt = [[SHA1Service sharedInstance] SHA1DecryptWithKey:keyData encryptedData:data withInitVector:vectorJson];
    
    return [[NSString alloc] initWithData:descrypt encoding:NSUTF8StringEncoding];
    
}

- (NSDictionary*) parsedJsonEncryptedData:(NSString*) encryptedData{
    
    NSString *textafterparsing = [encryptedData stringByReplacingOccurrencesOfString:@":aes256:" withString:@""];
    NSData *jsonData = [YAJL_GTMBase64 decodeString:textafterparsing];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];

    return json;
}
@end
