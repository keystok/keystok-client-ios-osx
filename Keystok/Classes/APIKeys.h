//
//  APIKeys.h
//  Keystok
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#ifndef ManakeyPOC_APIKeys_h
#define ManakeyPOC_APIKeys_h

#define kRequestKeystokPath  @"https://keystok.com"
#define kRequestKeystokAPIPath @"https://api.keystok.com"
#define kRequestKeystokAPIDestPath @"apps"
#define KRequestKeystokAuthDestPath @"oauth/token"

#define kDecryptionCT @"ct"     //Encrypted data
#define kDecryptionIV @"iv"     //Initialization vector
#define kDecryptionSalt @"salt" //Salt for PBKDF2
#define kDecryptionIter @"iter" //Iterations count for PBKDF2
#define kDecryptionKS @"ks"     //Key size (bits)

#define kIdentifierApplication @"id"
#define kDecriptionKey @"dk"
#define kRefreshToken @"rt"

#define kRequestUrlPath @"url_path"

#define kRequestRefreshToken @"refresh_token"
#define kRequestGrantType @"grant_type"
#define kRequestAccessToken @"access_token"
#define kRequestTokenType @"token_type"

#define kRequestKeystokAuth @"auth_host"
#define kRequestKeystokAPI  @"api_host"

#endif
