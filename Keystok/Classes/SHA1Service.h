//
//  AES256Service.h
//  Keystok
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import "KeystokImports.h"

@interface SHA1Service : NSObject
+ (id) sharedInstance;

//! Decryption method
/*!
 \param key represents derivative key
 \param data infromation that should be decrypted
 \param vector vector used for decryption method
 */
- (NSData *)SHA1DecryptWithKey:(NSData *)key encryptedData:(NSData*)data withInitVector:(NSString*) vector;


//! Derivative key
/*!
 \param password representing by string
 \param salt used for derivative key
 \param iterations The Pseudo Random Algorithm to use for the derivation
 iterations
 */
- (NSData *)SHA1KeyForPassword:(NSString *)password
                         salt:(NSData *)salt iterations:(uint) iterations;


@end
