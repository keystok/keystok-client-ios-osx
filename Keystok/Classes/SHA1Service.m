//
//  AES256Service.m
//  Keystok
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import "SHA1Service.h"
#import "GTMBase64.h"

@interface SHA1Service ()

@end

@implementation SHA1Service

+ (id) sharedInstance{
    
    static dispatch_once_t once;
    static id shared;
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
        
    });
    return shared;
    
}



- (NSData *)SHA1DecryptWithKey:(NSData *)key encryptedData:(NSData*)data withInitVector:(NSString*) vector{
    
    NSAssert(key!=nil, @"Key value need to be passed");
    NSAssert(data!=nil, @"Encrypted data need to be passed");
    NSAssert(vector!=nil, @"Vector value need to be passed");

    NSUInteger dataLength = [data length];
    
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    
    void *buffer = malloc(bufferSize);
    
    NSData *iv = [YAJL_GTMBase64 decodeString:vector];
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt,
                                          kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding,
                                          key.bytes,
                                          kCCKeySizeAES256,
                                          iv.bytes,
                                          [data bytes], dataLength,
                                          buffer,       bufferSize,
                                          &numBytesDecrypted);
    
    if (cryptStatus != kCCSuccess){
        MKLog(@"Unable to decrypt AES data");
    }
    
    NSData *result;
    
    result = [NSData dataWithBytes:(const void *)buffer length:(NSUInteger)numBytesDecrypted];
    
    free(buffer);
    return result;
    
}

- (NSData *)SHA1KeyForPassword:(NSString *)password
                         salt:(NSData *)salt iterations:(uint) iterations {
    
    NSAssert(password!=nil, @"Password value need to be passed");
    NSAssert(salt!=nil, @"Salt data need to be passed");
    NSAssert(iterations>0, @"Iteration should have correct value");

    
    NSMutableData *
    derivedKey = [NSMutableData dataWithLength:kCCKeySizeAES256];
    
    int
    keyDerivation = CCKeyDerivationPBKDF(kCCPBKDF2,
                                  password.UTF8String,
                                  [password lengthOfBytesUsingEncoding:NSUTF8StringEncoding],
                                  salt.bytes,
                                  salt.length,
                                  kCCPRFHmacAlgSHA1,
                                  iterations,
                                  derivedKey.mutableBytes,
                                  derivedKey.length);
    
    if(keyDerivation != kCCSuccess){
        MKLog(@"Unable to create AES key for password");
    }
    
    return derivedKey;
}
@end
