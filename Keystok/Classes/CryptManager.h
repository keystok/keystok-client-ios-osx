//
//  CryptManager.h
//  Keystok
//
//  Created by Jacek Grygiel on 05/02/14.
//  Copyright (c) 2014 sc5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeystokImports.h"

@interface CryptManager : NSObject

+ (id) sharedInstance;

//! Decryption sensitive data to string format
/*!
 \param json represents data with all needed values
 \param keyValue is sensitive string should be passed internally
 */

- (NSString*) decryptDataFromJSONDictionary:(NSDictionary*) json withKeyValue:(NSString*) keyValue;


//! Parsing method for parsing
/*!
 \param encryptedData fetch data
 \param keyValue
 */

- (NSDictionary*) parsedJsonEncryptedData:(NSString*) encryptedData;


@end
